# activity-lifecycle-state

# MyActivityLifecycle
Sample code logging main activity lifecycle methods
Main activity does not save state
Secondary activity does save state

Use of logcat to watch the logging

```
~$ <path to sdk install>/platform-tools/adb logcat -s LIFECYC
```
you can open two terminals and watch each in a different window:
```
~$ <path to sdk install>/platform-tools/adb logcat -s LIFECYC2
```
Take this code and modify it to have Activity2 launch Activity3
experiment and see what happens
